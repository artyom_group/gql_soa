import typing
import strawberry
import asyncio
import sqlalchemy
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker, declarative_base, scoped_session
from sqlalchemy.future import select
from storage import GameTbl, ScoreTbl, CommentTbl
import argparse

parser = argparse.ArgumentParser(description='DB name parser')
parser.add_argument('db_name', type=str, help='Database name')
parser.add_argument('host_name', type=str, help='Host name')
args = parser.parse_args()

engine = create_async_engine(
    "postgresql+asyncpg://postgres:root@" + args.host_name + "/" + args.db_name
)
async_session = sqlalchemy.orm.sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession
)


@strawberry.type
class Error:
    text: str


@strawberry.type
class Comment:
    author: str
    text: str


@strawberry.input
class AddComment:
    author: str
    text: str


@strawberry.type
class Scoreboard:
    players: typing.List[str]
    score: typing.List[str]


@strawberry.type
class Game:
    id: int
    active: bool

    @strawberry.field
    async def scoreboard(self) -> Scoreboard:
        async with async_session() as session, session.begin():
            stmt = select(ScoreTbl).filter_by(game_id=self.id)
            res = await session.execute(stmt)
            res = res.one()
            return Scoreboard(players=[res[0].player1, res[0].player2], score=[res[0].score1, res[0].score2])

    @strawberry.field
    async def comments(self) -> typing.List[Comment]:
        async with async_session() as session, session.begin():
            stmt = select(CommentTbl).filter_by(game_id=self.id)
            res = await session.execute(stmt)
            ans = []
            for comment in res.scalars():
                ans.append(Comment(author=comment.author, text=comment.text))
        return ans


@strawberry.type
class Query:
    @strawberry.field
    async def games(self) -> typing.List[Game]:
        async with async_session() as session, session.begin():
            stm = select(GameTbl)
            res = await session.execute(stm)
            ans = []
            for game in res.scalars():
                ans.append(Game(id=game.id, active=game.active))
        return ans


@strawberry.type
class Mutation:
    @strawberry.field
    async def add_comment(self, game_id: int, comment: AddComment) -> typing.Optional[Comment]:
        try:
            async with async_session() as session, session.begin():
                session.add_all(
                    [
                        CommentTbl(text=comment.text, author=comment.author, game_id=game_id)
                    ]
                )
        except sqlalchemy.exc.IntegrityError:
            return None
        return Comment(author=comment.author, text=comment.text)


@strawberry.type
class Subscription:
    @strawberry.subscription
    async def scoreboard(self, gameId: int) -> typing.AsyncGenerator[Scoreboard, None]:
        while True:
            async with async_session() as session, session.begin():
                stm = select(GameTbl).filter_by(id=gameId).options(sqlalchemy.orm.selectinload(GameTbl.scoreboard))
                res = await session.execute(stm)
                res = res.one()
                yield Scoreboard(players=[res[0].scoreboard[0].player1, res[0].scoreboard[0].player2],
                                 score=[res[0].scoreboard[0].score1, res[0].scoreboard[0].score2])
            await asyncio.sleep(2)


schema = strawberry.Schema(query=Query, mutation=Mutation, subscription=Subscription)
