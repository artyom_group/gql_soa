import sqlalchemy
from sqlalchemy.orm import sessionmaker, declarative_base, scoped_session
from sqlalchemy_utils import create_database, database_exists
import argparse

Base = declarative_base()


class GameTbl(Base):
    __tablename__ = 'games'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    active = sqlalchemy.Column(sqlalchemy.Boolean)
    scoreboard = sqlalchemy.orm.relationship("ScoreTbl")
    comment = sqlalchemy.orm.relationship("CommentTbl")


class ScoreTbl(Base):
    __tablename__ = 'scores'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    game_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey("games.id"))
    player1 = sqlalchemy.Column(sqlalchemy.String)
    player2 = sqlalchemy.Column(sqlalchemy.String)
    score1 = sqlalchemy.Column(sqlalchemy.Integer)
    score2 = sqlalchemy.Column(sqlalchemy.Integer)


class CommentTbl(Base):
    __tablename__ = 'comments'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    author = sqlalchemy.Column(sqlalchemy.String)
    text = sqlalchemy.Column(sqlalchemy.String)
    game_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey("games.id"))


parser = argparse.ArgumentParser(description='DB name parser')
parser.add_argument('db_name', type=str, help='Database name')
parser.add_argument('host_name', type=str, help='Host name')

if __name__ == "__main__":
    args = parser.parse_args()
    if not database_exists("postgresql://postgres:root@" + args.host_name + "/" + args.db_name):
        create_database("postgresql://postgres:root@" + args.host_name + "/" + args.db_name)
    engine = sqlalchemy.create_engine("postgresql://postgres:root@" + args.host_name + "/" + args.db_name)
    Base.metadata.create_all(bind=engine)
